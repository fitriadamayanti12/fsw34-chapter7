// Asynchronous process
console.log("Hello guys!");

// Output code will be delay 100 millisecond
setTimeout(() => {
  console.log("Now study Asynchronous");
}, 100);

console.log("I'm a developer");
