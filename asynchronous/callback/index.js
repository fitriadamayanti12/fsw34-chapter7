const fs = require("fs");
const option = { encoding: "utf-8" };

const callback = (err, data) => {
  console.log("I'm the second");
  if (err) return console.log("error:", err.message);
  console.log("My file:", data);
};

fs.readFile("hello.txt", option, callback);

console.log("I'm the first");
