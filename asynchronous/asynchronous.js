// const p = document.querySelector(".p");
// setTimeout(() => {
//   p.textContent = "My name is Sabrina!";
// }, 5000);
// p.getElementsByClassName.color = "red";

console.log("Hello Binarian!");
setTimeout(() => {
  console.log("Javascript");
}, 100);
console.log("Developer");

/**
 * Asynchronous
 *  Asynchronous code is executed after a task that runs in "backgound" finish
 * Execution doesn't wait for an asynchronous tasks to finish its work
 * Callback functions alone do NOT make code asynchronous
 * Callback will run after timer
 */

/**
 * 4 task
 * 1 ==> 2 second
 * 2 ==> 2 second, 3, 4
 * 4 second
 */
