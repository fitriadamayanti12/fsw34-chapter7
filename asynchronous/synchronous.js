// const p = document.querySelector(".p");
// p.textContent = "My name is Sabrina!";
// alert("Text sent!");
// p.style.color = "red";

/**
 * Sychronous Code
 * - Most code is synchronous
 * - Each line of code waits for previous line to finish
 * - Long-running operations block code execution
 */

/**
 * 4 command
 * 1 ==> 2 second
 * 2 ==> 2 second
 * 3 ==> 1 second
 * 4 ==> 1 second
 * 6 second
 */

console.log("Hello Binarian!");
console.log("Javascript");
console.log("Developer");
