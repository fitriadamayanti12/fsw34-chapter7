const router = require("express").Router();
const home = require("./controllers/homeController");
const user = require("./controllers/userController");

//Get data
router.get("/", home.index);
router.get("/users", user.index);
router.get("/users/new", user.new);

// Create
router.post("/users", user.create);

// Update
router.get("/users/form-update/:uuid", user.getFormUpdate);
router.post("/users/update/:uuid", user.update);

// Delete
router.get("/users/delete/:uuid", user.delete);

module.exports = router;

// File in tidak digunakan ketika menerapkan MCR
