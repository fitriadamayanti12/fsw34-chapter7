const { User } = require("../models");

module.exports = {
  // View Controller

  index: (req, res) => {
    User.findAll().then((users) => {
      res.render("users/index", {
        users,
      });
    });
  },

  // Form input a user
  new: (req, res) => {
    res.render("users/create");
  },

  // Create a new user
  create: (req, res) => {
    User.create({
      name: req.body.name,
      email: req.body.email,
      role: req.body.role,
    }).then((users) => {
      res.send("User berhasil dibuat");
    });
  },

  // Delete an user
  delete: (req, res) => {
    User.destroy({
      where: { uuid: req.params.uuid },
    }).then((users) => {
      res.render("users/delete");
    });
  },

  // Form update an user
  getFormUpdate: (req, res) => {
    User.findOne({
      where: { uuid: req.params.uuid },
    }).then((users) => {
      res.render("users/update", {
        uuid: users.uuid,
        name: users.name,
        email: users.email,
        role: users.role,
      });
    });
  },

  // Update user by uuid
  update: (req, res) => {
    User.update(
      {
        name: req.body.name,
        email: req.body.email,
        role: req.body.role,
      },
      {
        where: { uuid: req.params.uuid },
      }
    )
      .then((users) => {
        res.send("User berhasil diupdate");
      })
      .catch((err) => {
        res.status(500).json("Can't update an user");
      });
  },
};
