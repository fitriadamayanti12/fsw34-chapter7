const router = require("express").Router();
const home = require("./home");
const user = require("./user");

router.use("/", home);
router.use("/users", user);

module.exports = router;
