const router = require("express").Router();
const userContoller = require("../controllers/userController");

//Get data
router.get("/", userContoller.index);

// Create
router.get("/new", userContoller.new);
router.post("/", userContoller.create);

// Update
router.get("/form-update/:uuid", userContoller.getFormUpdate);
router.post("/update/:uuid", userContoller.update);

// Delete
router.get("/delete/:uuid", userContoller.delete);

module.exports = router;
