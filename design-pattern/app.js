const express = require("express");
const { PORT = 3000 } = process.env;

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.set("view engine", "ejs");

// Import router
const router = require("./routes/index");
app.use(router);

// Server listen on port 3000
app.listen(PORT, async () => {
  console.log(`Server up on htpp://localhost:${PORT}`);
  console.log("Database Connected!");
});
