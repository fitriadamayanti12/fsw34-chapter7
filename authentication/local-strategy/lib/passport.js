const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const { User } = require("../models");

/** Function for authentication */
async function authenticate(username, password, done) {
  try {
    // Call method
    const user = await User.authenticate({ username, password });

    /**
     * done adalah callback, parameter pertamanya adalah error,
     * jika tidak ada error maka kita beri null saja.
     * Parameter keduanya adalah data yang nantinya dapa
     * kita akses di dalam req.user
     */
    return done(null, user);
  } catch (err) {
    // Parameter ketika akan dilempar ke dalam flash
    return done(null, false, { message: err.message });
  }
}

passport.use(
  new LocalStrategy(
    { usernameField: "username", passwordField: "password" },
    authenticate
  )
);

/**
 * Serialize & Deserialize
 * Cara untuk membuat sesi dan delete sesi
 */
passport.serializeUser((user, done) => done(null, user.id));
passport.deserializeUser(function (id, done) {
  User.findOne({ where: { id: id } }).then((user) => {
    done(null, user);
  });
});

module.exports = passport;
