1. npm install -g express-generator
2. express --view ejs --git local-strategy
3. Masuk ke direktori: cd nama_project
4. npm install express-session express-flash sequelize pg
5. sequelize init
6. psql -U username & enter password
7. CREATE USER user-name WITH PASSWORD 'admin';
8. ALTER USER user-name WITH SUPERUSER;
9. config db in file config.json
10. sequelize db:create
11. sequelize model:generate --name User --attributes username:string,password:string
12. sequelize db:migrate
13. npm install bcrypt
14. npm install passport passport-local
15. npm install jsonwebtoken
16. npm install passport passport-jwt