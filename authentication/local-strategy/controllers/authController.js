const { User } = require("../models");
const passport = require("../lib/passport");

module.exports = {
  register: (req, res, next) => {
    // Call static method
    User.register(req.body)
      .then(() => {
        res.redirect("/login");
      })
      .catch((err) => next(err));
  },
  login: passport.authenticate("local", {
    successRedirect: "/dashboard",
    failureRedirect: "/login",
    failureFlash: true,
  }),
  whoami: (req, res) => {
    // req.user adalah isntance dari User model. hasil autentikasi
    res.render("profile", req.user.dataValues);
  },
};
