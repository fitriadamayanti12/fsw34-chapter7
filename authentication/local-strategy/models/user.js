"use strict";
const { Model } = require("sequelize");
// Import bcrypt untuk melakukan encription
const bcrypt = require("bcrypt");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    // Method for encription
    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    // Method register
    static register = ({ username, password }) => {
      const encryptedPassword = this.#encrypt(password);

      /**
       * #encrypt static method
       * encryptedPassword sama dengan string hasil dari enkripsi
       * password dati method #encrypt
       */
      return this.create({ username, password: encryptedPassword });
    };

    // Check password
    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    // Method Login
    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        if (!user) return Promise.reject("User not found");
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Wrong password");
        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }
  User.init(
    {
      username: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "users",
      modelName: "User",
    }
  );
  return User;
};
