var express = require("express");
var router = express.Router();
const home = require("../controllers/homeController");
const auth = require("../controllers/authController");

/* GET home page. */
router.get("/", home.index);
router.get("/dashboard", home.dashboard);

// Register
router.get("/register", (req, res) => res.render("register"));
router.post("/register", auth.register);

// Login
router.get("/login", (req, res) => res.render("login"));
router.post("/login", auth.login);

// Middlewares for protect
const restrict = require("../middlewares/restrict");
router.get("/dashboard", restrict, (req, res) => res.render("dashboard"));

// Profile
router.get("/whoami", restrict, auth.whoami);

// Endpoint generate token
router.post("/api/v1/auth/loagin", auth.login);
router.get("/api/v1/auth/whoami", restrict, auth.whoami);

module.exports = router;
