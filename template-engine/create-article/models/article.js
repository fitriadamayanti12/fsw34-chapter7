const articles = [
  {
    id: 1,
    title: "What is Lorem Ipsum?",
    author: "John Doe",
    body: `Lorem Ipsum is simply dummy text of the printing and typesetting industry.`,
    approved: true,
  },
  {
    id: 2,
    title: "Why do we use it?",
    author: "Sabrina",
    body: `It is a long established fact.`,
    approved: true,
  },
  {
    id: 3,
    title: "Hello World",
    author: "Agatha",
    body: "Cuma lorem ipsum aja kok",
    approved: false,
  },
];
module.exports = {
  findAll: () => Promise.resolve(articles),
  create: ({ title, author, body }) => {
    const id = articles[articles.length - 1].id + 1;
    const article = { id, title, author, body };
    articles.push(article);
    return Promise.resolve(article);
  },
};
