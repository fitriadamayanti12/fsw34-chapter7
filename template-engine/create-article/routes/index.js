var express = require("express");
var router = express.Router();
const pagesRouter = require("./pages");

router.use(pagesRouter);

module.exports = router;
