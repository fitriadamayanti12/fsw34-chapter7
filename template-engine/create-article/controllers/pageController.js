exports.home = (req, res) => {
  const title = "Hello World",
    subTitle = "Welcome to my website";

  res.render("index", { title, subTitle });
};

exports.about = (req, res) => {
  res.render("about", { title: "About" });
};

const { Article } = require("../models");

exports.articles = (req, res) => {
  Article.findAll().then((articles) => {
    res.render("articles", { title: "Articles", articles });
  });
};
